var express = require('express');
var router = express.Router();
var boardSvc = require('../service/boardSvc');
var wrap = require("../module/func").wrapSuccess;
var errors = require('../module/errors').toError;
router.post('/write',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.content = req.body.content;
  data.title = req.body.title;

  boardSvc.createBoard(data,req.files.photo)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/modify',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = req.body.idx;
  data.content = req.body.content;
  data.title = req.body.title;

  boardSvc.modifyBoard(data,req.files.photo)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })

})

router.post('/delete',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = req.body.idx;
  boardSvc.deleteBoard(data,req.files.photo)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})



router.post('/writeComment', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.grp = req.idx;
  data.content = req.body.content;
  boardSvc.createComment(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/modifyReview', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = data.idx;
  data.content = req.body.content;
  boardSvc.modifyComment(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/deleteComment', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = data.idx;
  boardSvc.deleteComment(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/goodComment', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.grp = data.idx;
  boardSvc.goodComment(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})

router.post('/delGoodComment', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = data.idx;
  boardSvc.delGoodComment(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
});


module.exports = router;
