var express = require('express');
var router = express.Router();
var messageSvc = require('../service/messageSvc');
var _ = require('lodash');
var errors = require('../module/errors').toError;

router.post('/send',function(req,res){
  var data = {};
  data.id = req.sesion.profile.id;
  data.from = data.id;
  data.content = req.body.content;
  data.to = req.body.targetId;
  messageSvc.send(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/list',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  messageSvc.getList(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
});

router.post('/read',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.id = req.body.idx;
  messageSvc.read(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/notice',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  messageSvc.notice(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })

})



module.exports = router;
