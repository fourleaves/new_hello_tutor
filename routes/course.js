var express = require('express');
var router = express.Router();
var courseSvc = require('../service/courseSvc');
var wrap = require("../module/func").wrapSuccess;
var errors = require('../module/errors').toError;

router.post('/write',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.content = req.body.content;
  data.title = req.body.title;
  data.one_line = req.body.one_line;

  courseSvc.createCourse(data,req.files.photo)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/modify',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = req.body.idx;
  data.content = req.body.content;
  data.title = req.body.title;
  data.one_line = req.body.one_line;

  courseSvc.modifyCourse(data,req.files.photo)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })

})

router.post('/delete',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = req.body.idx;
  courseSvc.deleteCourse(data,req.files.photo)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})



router.post('/writeReview', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.grp = req.idx;
  data.content = req.body.content;
  data.title = req.body.title;
  courseSvc.createReview(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/modifyReview', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = data.idx;
  data.content = req.body.content;
  data.title = req.body.title;
  courseSvc.modifyReview(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/deleteReview', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = data.idx;
  courseSvc.deleteReview(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/goodReview', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.grp = data.idx;
  courseSvc.goodReview(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})

router.post('/delGoodReview', function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = data.idx;
  courseSvc.delGoodReview(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
});


module.exports = router;
