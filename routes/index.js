var express = require('express');
var router = express.Router();


//들어오는 모든 요청에 대하여 기록한다.
router.all('*', function(req, res, next) {
  var ip = req.ip;
//    console.log(req.sessionID);
  var record = {};
  record.ip = req.ip;
  record.sessionID = req.sessionID;
  record.path = req.path;
  record.user_agent = req.header('user-agent');
  LOG.request('asdf',record);
  next();
});

module.exports = router;
