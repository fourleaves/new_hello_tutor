var express = require('express');
var router = express.Router();
var userSvc = require('../service/userSvc');
var wrap = require("../module/func").wrapSuccess;
var errors = require('../module/errors').toError;

router.post('/joinTutor',function(req,res,next){
  var param = req.body;
  var data = {};
  data.email = param.email;
  data.pw = param.pw;
  data.isTutor = 1;
  data.last_name = param.last_name;
  data.first_name = param.first_name;
  data.gender = param.gender;
  data.birthday = param.birthday;
  data.address_si = param.address_si;
  data.address_gu = param.address_gu;
  data.subjects = param.subjects;
  data.about_me = param.about_me;
  console.log(data);
  console.log(req.files.photo);
  userSvc.signupTutor(data,req.files.photo)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })

})

router.post('/joinTutee',function(req,res,next){
  var param = req.body;
  var data = {};
  data.email = param.email;
  data.pw = param.pw;
  data.isTutor = 0;
  data.last_name = param.last_name;
  data.first_name = param.first_name;
  data.gender = param.gender;
  data.birthday = param.birthday;
  data.address_si = param.address_si;
  data.address_gu = param.address_gu;
  data.subjects = param.subjects;
  data.about_me = param.about_me;
  data.school_name = param.school_name;
  data.grade = param.grade;
  userSvc.signupTutee(data,req.files.photo)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })

})


router.post('/login',function(req,res,next){
  var param = req.body;
  var data = {};
  data.email = param.email;
  data.pw = param.pw;
  userSvc.login(data)
  .then(function(result){
    console.log(result);
    req.session.profile = result;
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })

})



router.post('/recommendUser',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  userSvc.recommendUser(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
})

router.post('/recommendTutor',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  userSvc.recommendTutor(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })

});

router.post('/logout',function(req,res) {
    req.session.destroy();
    res.json({isSuccess:1});
})

router.post('/tutorSimple', function(req, res) {
  var data = {};
  data.id = req.session.profile.id;
  data.targetId = req.body.id;
  userSvc.getTutorSimple(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })

});

router.post('/tuteeSimple', function(req, res) {
  var data = {};
  data.id = req.session.profile.id;
  data.targetId = req.body.id;
  userSvc.getTuteeSimple(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })

});


router.post('/tutorDetail', function(req, res){
  var data = {};
  data.id = req.session.profile.id;
  data.targetId = req.body.id;
  userSvc.getTutorDetail(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
});


router.post('/writeCareer', function(req, res){
  var data = {};
  data.id = req.session.profile.id;
  data.title = req.body.title;
  data.fromDate = req.body.fromDate;
  data.toDate = req.body.toDate;
  data.content = req.body.content;
  userSvc.writeCareer(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
});


router.post('/deleteCareer', function(req, res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = req.body.idx;
  userSvc.deleteCareer(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
});

router.post('/modifyCareer', function(req, res){
  var data = {};
  data.id = req.session.profile.id;
  data.idx = req.body.idx;
  userSvc.modifyCareer(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
});

router.post('/deleteSubject', function(req, res){
  data.id = req.session.profile.id;
  data.subjectCode = req.body.subjectCode;
  userSvc.deleteSubject(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
});

router.post('/addSubject', function(req, res){
  data.id = req.session.profile.id;
  data.subjectCode = req.body.subjectCode;
  userSvc.addSubject(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
});

router.post('/checkEmail', function(req, res){
  data.id = req.session.profile.id;
  data.subjectCode = req.body.subjectCode;
  userSvc.checkEmail(data)
  .then(function(result){
    res.json(wrap(result));
  })
  .catch(function(err){
    next(err);
  })
});



module.exports = router;
