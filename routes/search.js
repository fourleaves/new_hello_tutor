var express = require('express');
var router = express.Router();
var searchSvc = require('../service/searchSvc');
var errors = require('../module/errors').toError;



router.post('/tutee',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.keyword = req.body.keyword;
  data.page = req.body.page;
  searchSvc.searchTutee(data)
    .then(function(result){
      res.json(warp(result));
    })
    .catch(function(err){
      next(err);
    })
})

router.post('/tutor',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.keyword = req.body.keyword;
  data.page = req.body.page;
  searchSvc.searchBoard(data)
    .then(function(result){
      res.json(warp(result));
    })
    .catch(function(err){
      next(err);
    })
})

router.post('/course',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.keyword = req.body.keyword;
  data.page = req.body.page;
  searchSvc.searchTutor(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })

})
router.post('/board',function(req,res){
  var data = {};
  data.id = req.session.profile.id;
  data.keyword = req.body.keyword;
  data.page = req.body.page;
  searchSvc.searchBOard(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })

})

module.exports = router;
