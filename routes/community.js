var express = require('express');
var router = express.Router();
var userSvc = require('../service/userSvc');
var wrap = require("../module/func").wrapSuccess;
var errors = require('../module/errors').toError;

router.post('/follow',function(req,res){
  var data = {};
  data.follower = req.body.profile.id;
  data.feeder = req.body.target;
  userSvc.follow(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})

router.post('/unfollow',function(req,res){
  var data = {};
  data.follower = req.body.profile.id;
  data.feeder = req.body.target;
  userSvc.unfollow(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/reqFriend',function(req,res){
  var data = {};
  data.id = req.body.profile.id;
  data.friend = req.body.friend;
  userSvc.reqFriend(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


router.post('/acceptFriend',function(req,res){
  var data = {};
  data.id = req.body.profile.id;
  data.friend = req.body.friend;
  userSvc.acceptFriend(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})

router.post('/rejectFriend',function(req,res){
  var data = {};
  data.id = req.body.profile.id;
  data.friend = req.body.friend;
  userSvc.rejectFriend(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})

router.post('/friendList',function(req,res){
  var data = {};
  data.id = req.body.profile.id;
  userSvc.getFriendList(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})

router.post('/followList',function(req,res){
  var data = {};
  data.id = req.body.profile.id;
  userSvc.getFollowList(data)
  .then(function(result){
    res.json(warp(result));
  })
  .catch(function(err){
    next(err);
  })
})


module.exports = router;
