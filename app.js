var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var redis = require('redis');
var multer = require('multer');
var redisStore = require('connect-redis')(session);
var client = redis.createClient();


var app = express();

var serverInit = function(callback){


    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');

    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));

    app.use(session(
        {
            secret: 'yourothersecretcode',
            store: new redisStore({ host: "127.0.0.1", port: 6379, client: client }),
            saveUninitialized: false, // don't create session until something stored,
            resave: false // don't save session if unmodified
        }
    ));


    var routes = require('./routes/index');
    var users = require('./routes/users');
    var community = require('./routes/community');
    var course = require('./routes/course');
    var board = require('./routes/board');
    var search = require('./routes/search');
    var messages = require('./routes/messages');
    app.use(multer({}));

    app.use('/', routes);
    app.use('/users', users);
    app.use('/community', community);
    app.use('/course', course);
    app.use('/board', board);
    app.use('/search', search);
    app.use('/message', messages);




    var router = express.Router();


    // production error handler
    router.get('/session/set/:value', function(req, res) {
        req.session.redSession = req.params.value;
        res.send('session written in Redis successfully');
    });

    app.get('/session/get/', function(req, res) {
        if(req.session.redSession)
            res.send('the session value stored in Redis is: ' + req.session.redSess);
        else
            res.send("no session value stored in Redis ");
    });


    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
      var err = new Error('Not Found');
      err.status = 404;
      next(err);
    });

    // error handlers

    // development error handler
    // will print stacktrace
    // if (app.get('env') === 'development') {
    //   app.use(function(err, req, res, next) {
    //     res.status(err.status || 500);
    //     res.render('error', {
    //       message: err.message,
    //       error: err
    //     });
    //   });
    // }

    // production error handler
    // no stacktraces leaked to user
    var toError = require('./errors').toError;
    app.use(function(err, req, res, next) {
      // res.status(err.status || 500);
      // res.render('error', {
      //   message: err.message,
      //   error: {}
      // });
      LOG.error(err);
      res.json(toError(err));
    });


    var server = app.listen(80, function() {
        console.log('서버가 '+server.address().port+' port에서 실행중...');
        if(callback){
            callback();
        }
    });
}


module.exports = serverInit;
