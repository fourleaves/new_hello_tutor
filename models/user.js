/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
        primaryKey: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    school_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    grade: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    }
  }, {
    tableName: 'user'
  });
};
