/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('course_subject', {
    grp: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    subjectCode: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'subject_list',
        key: 'code'
      }
    }
  }, {
    tableName: 'course_subject'
  });
};
