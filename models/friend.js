/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('friend', {
    friend1: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
        primaryKey: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    friend2: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
        primaryKey: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    agree: {
      type: 'CHAR(1)',
      allowNull: true,
      defaultValue: 'n'
    },
    regdate: {
      type: DataTypes.DATE,
      allowNull: true,
        defaultValue: DataTypes.NOW
    },
    score: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
        defaultValue: '1'
    }
  }, {
    tableName: 'friend'
  });
};
