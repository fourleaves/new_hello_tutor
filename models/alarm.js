/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('alarm', {
    idx: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    receiver: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    actor: {
      type: DataTypes.INTEGER(11),
      allowNull : true
    },
    course : {
      type : DataTypes.INTEGER(11),
      allowNull : true
    },
    board : {
      type : DataTypes.INTEGER(11),
      allowNull : true
    },
    read_yn : {
      type: 'CHAR(1)',
      allowNull: true,
      defaultValue: 'n'
    },
    regdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: DataTypes.NOW
    },
    cate: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
    }
  }, {
    tableName: 'alarm'
  });
};
