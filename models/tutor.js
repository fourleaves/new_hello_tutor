/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tutor', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    academic: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'tutor'
  });
};
