/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('review_good', {
    grp: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
        primaryKey: true,
      references: {
        model: 'review',
        key: 'idx'
      }
    },
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
        primaryKey: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    regdate: {
      type: DataTypes.DATE,
      allowNull: true,
        defaultValue: DataTypes.NOW
    }
  }, {
    tableName: 'review_good'
  });
};
