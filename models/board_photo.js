/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('board_photo', {
    idx: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    grp: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    grp_num: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    photo: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    tableName: 'board_photo'
  });
};
