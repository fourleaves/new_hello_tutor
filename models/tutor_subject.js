/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tutor_subject', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
        primaryKey: true,
      references: {
        model: 'tutor',
        key: 'id'
      }
    },
    subjectCode: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
        primaryKey: true,
      references: {
        model: 'subject_list',
        key: 'code'
      }
    }
  }, {
    tableName: 'tutor_subject'
  });
};
