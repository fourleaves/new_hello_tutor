/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('subject_list', {
    code: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    subject: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: 'subject_list'
  });
};
