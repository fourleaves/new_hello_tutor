/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('career', {
    idx: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'tutor',
        key: 'id'
      }
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    fromdate: {
      type: DataTypes.DATE,
      allowNull: false,
        defaultValue: DataTypes.NOW
    },
    todate: {
      type: DataTypes.DATE,
      allowNull: false,
        defaultValue: DataTypes.NOW
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: 'career'
  });
};
