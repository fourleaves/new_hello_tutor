/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('alarm_relation', {
    idx: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    receiver: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    from: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    to: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    regdate: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: DataTypes.NOW
    },
    receive_yn: {
      type: 'CHAR(1)',
      allowNull: true,
      defaultValue: 'n'
    }
  }, {
    tableName: 'alarm_relation'
  });
};
