/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('message', {
    idx: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    from: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    to: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    content: {
      type: DataTypes.STRING,
      allowNull: true
    },
    senddate: {
      type: DataTypes.DATE,
      allowNull: true,
        defaultValue: DataTypes.NOW
    },
    read_ok: {
      type: 'CHAR(1)',
      allowNull: true,
      defaultValue: 'n'
    }
  }, {
    tableName: 'message'
  });
};
