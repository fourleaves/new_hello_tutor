/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('follow', {
    feeder: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
        primaryKey: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    follower: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
        primaryKey: true,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    regdate: {
      type: DataTypes.DATE,
      allowNull: true,
        defaultValue: DataTypes.NOW
    }
  }, {
    tableName: 'follow'
  });
};
