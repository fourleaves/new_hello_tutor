/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    pw: {
      type: DataTypes.STRING,
      allowNull: false
    },
    salt: {
        type: DataTypes.STRING,
        allowNull: false
      },
    isTutor: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    last_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    first_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: true
    },
    photo: {
      type: DataTypes.STRING,
      allowNull: true
    },
    address_si: {
      type: DataTypes.STRING,
      allowNull: false
    },
    address_gu: {
      type: DataTypes.STRING,
      allowNull: false
    },
    gender: {
      type: 'CHAR(1)',
      allowNull: false
    },
    birthday: {
      type: DataTypes.DATE,
      allowNull: false
    },
    about_me: {
      type: DataTypes.STRING,
      allowNull: false
    },
    leave_yn: {
      type: 'CHAR(1)',
      allowNull: true,
      defaultValue: '0'
    },
    regdate: {
      type: DataTypes.DATE,
      allowNull: true,
        defaultValue: DataTypes.NOW
    }
  }, {
    tableName: 'users'
  });
};
