/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('board', {
    idx: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: true
    },
    video1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    video2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    url1: {
      type: DataTypes.STRING,
      allowNull: true
    },
    url2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    regdate: {
      type: DataTypes.DATE,
      allowNull: true,
        defaultValue: DataTypes.NOW
    },
    del_yn: {
      type: 'CHAR(1)',
      allowNull: true,
      defaultValue: 'n'
    },
    update: {
      type: DataTypes.DATE,
      allowNull: true,
        defaultValue: DataTypes.NOW
    }
  }, {
    tableName: 'board'
  });
};
