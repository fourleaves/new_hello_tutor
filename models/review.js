/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('review', {
    idx: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    grp: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'course',
        key: 'idx'
      }
    },
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      }
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false
    },
    regdate: {
      type: DataTypes.DATE,
      allowNull: true,
        defaultValue: DataTypes.NOW
    }
  }, {
    tableName: 'review'
  });
};
