'use strict';

var server = require('./app.js');
var nconf = require('nconf');
var async = require('async');


// Set up configs
nconf.use('memory');
// First load command line arguments
nconf.argv();
// Load environment variables
nconf.env();
// Load config file for the environment
nconf.file('./config_dev.json');

//log init;
require('./module/logger.js')();


console.log('[APP] Starting server initialization');

// Initialize Modules
async.series([

        function startServer(callback) {
            server(callback);
        },
        // function dbInit(callback){
        //     require('./service/sequelize').(function(err){
        //         console.log('server_matching queue start',err);
        //         callback(err);
        //     });
        // },
        function (callback) {
            var need_init = false;
            if(need_init) require('./service/init').init(callback);
            else callback(null);
        }], function(err) {
            if (err) {
                console.log('[APP] initialization failed', err);
            } else {
                console.log('[APP] initialized SUCCESSFULLY');
            }
    }
);
