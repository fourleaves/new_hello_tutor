var winston = require('winston');
require('winston-mongodb').MongoDB;
var nconf = require("nconf");

var globalLogInit = function(){
    var myCustomLevels = nconf.get("errLevels");


    var LOG=  new (winston.Logger)({
      levels: myCustomLevels.levels,
      transports: [
          new winston.transports.Console({
              level : "logg"
          }),
          new (winston.transports.MongoDB)({
              level:'logg',
              db:nconf.get("mongodb"),
              collection:'logs'
          }),
      ],
      exceptionHandlers: [
          new winston.transports.Console({
              json : true
          }),
          new winston.transports.MongoDB({
              db:nconf.get("mongodb"),
              collection: 'exception'
          })
      ]
    });

    global.LOG = LOG;
};
//logger.global_log_init = global_log_init;
module.exports = globalLogInit;
