var wrapSuccess = function(message){
    var sendMessage = {};
    sendMessage.success = 1;
    sendMessage.result = message;
    return sendMessage;
}

var toDataSequelize = function(data){
  return JSON.parse(JSON.stringify(data));
}

exports.wrapSuccess = wrapSuccess;
exports.toDataSequelize = toDataSequelize;
