var toTrack = require('stack-trace');
var _ = require('lodash');

module.exports = function CustomError(errSet, sql,extra) {
    Error.captureStackTrace(this);
    this.message = errSet.message;
    this.errno = errSet.errno;
    this.sql = sql || undefined;
    this.extra = extra || undefined;
    this.trace = toTrack.parse(this);
//    this.errorName = errorName;
};

module.exports.dic = {
    //mysql error
    ERROR : {
        errno: 1048,
        message: "parameter is not set"
    }
}

module.exports.toError = function(err){
    // if(err.sqlState != undefined){
    //     var messages = _.find(module.exports.dic, function(o) {
    //         return o.errno  == err.sqlState || o.errno == err.errno;
    //     });
    //     var newError = {};
    //     if(messages == undefined || messages == null){
    //         messages = {};
    //         messages.message = "unknown";
    //         messages.errno = 9999;
    //         newError.mysql = err;
    //     }
    //     if(err.stack){
    //         newError.stack = err.stack;
    //         newError.track = toTrack.parse(err);
    //     }
    //     newError.message = messages.message;
    //     newError.errno = messages.errno;
    //
    //     return newError;
    // }
    // else return err;
    return { success : 0 };
}
