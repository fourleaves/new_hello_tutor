exports.searchTutee = "select st1.id,st1.email,st1.first_name,st1.last_name,st1.school_name,st1.grade,st1.thumbnail, ifnull(rcm.score,0)+if(st1.address_si = $address_si,1,0)+if(st1.school_name = $school_name,1,0) as score, rcm.together "
     + "from stu_info st1 left outer join recommends rcm on st1.id = rcm.friend2 and rcm.friend1 = $id left outer join stu_info st2 ON st2.id = $id where st1.first_name like $keyword and st1.id <> $id";

exports.searchTutor = "select st1.id,st1.email,st1.first_name,st1.last_name,st1.school_name,st1.grade,st1.thumbnail, ifnull(rcm.score,0)+if(st1.address_si = $address_si,1,0) as score, rcm.together "
    + "from user_info st1 left outer join recommends rcm on st1.id = rcm.friend2 and rcm.friend1 = $id left outer join user_info st2 ON st2.id = $id where st1.first_name like $keyword and st1.id <> $id"


exports.recommendUser =
"select recommend.email, info.name,0 as isTutor, info.thumbnail,info.address_si,info.address_gu, info.school_name, info.grade, sum(score) as score, sum(together) as together   from (    " +
"select u2.id, 1 as score, 0 as together " +
"from stu_info u1,stu_info u2 " +
"where u1.id = $id " +
"and u1.address = u2.address " +
"and u1.age = u2.age " +
"and u2.id <> $id " +
"union all " +
"select u2.id, 1 as score, 0 as together " +
"from user u1,user u2 " +
"where u1.id = $id " +
"and u1.school_name = u2.school_name " +
"and u2.id <> $id " +
"union all " +
"select friend2 as id, score, together " +
"from recommend_duple " +
"where friend1 = $id) recommend join stu_info info " +
"on recommend.id = info.id " +
"where recommend.id not in (select friend2 from friends WHERE friend1 = $id) " +
"group by recommend.email " +
"order by score desc " +
"limit $limitNum,$size";

exports.recommendTutor =
"select recommend.email, info.name,1 as isTutor, info.thumbnail,info.address_si,info.address_gu, sum(score) as score, sum(together) as together " +
"from ( " +
"select u2.id, 1 as score, 0 as together " +
"from user_info u1,user_info u2 " +
"where u1.id = $id " +
"and u1.address = u2.address " +
"and u2.id <> $id " +
"union all " +
"select friend2 as id, score, together " +
"from recommends " +
"where friend1 = $id) recommend join user_info info " +
"on recommend.id = info.id " +
"where info.isTutor = '1' " +
"and recommend.id not in (select friend2 from friend_list2 WHERE friend1 = $id) " +
"group by recommend.id " +
"order by score desc " +
"limit $limitNum,$size";
