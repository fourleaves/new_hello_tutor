var Sequelize = require("sequelize");
var path = require("path");
var nconf = require("nconf");
var sequelizeData = nconf.get("mysql");
var sequelize = new Sequelize(
  sequelizeData.database,
  sequelizeData.user,
  sequelizeData.password,
  {
    dialect: 'mysql',
    pool : sequelizeData.pool,
    define: {
        timestamps: false
    }
  }
);

var Users = sequelize.import(path.join(__dirname ,"..","models","users"));
var Alarm = sequelize.import(path.join(__dirname ,"..","models","alarm"));
var Board = sequelize.import(path.join(__dirname ,"..","models","board"));
var Board_comment = sequelize.import(path.join(__dirname ,"..","models","board_comment"));
var Board_good = sequelize.import(path.join(__dirname ,"..","models","board_good"));
var Board_photo = sequelize.import(path.join(__dirname ,"..","models","board_photo"));
var Career = sequelize.import(path.join(__dirname ,"..","models","career"));
var Course = sequelize.import(path.join(__dirname ,"..","models","course"));
var Course_subject = sequelize.import(path.join(__dirname ,"..","models","course_subject"));
var Follow = sequelize.import(path.join(__dirname ,"..","models","follow"));
var Friend = sequelize.import(path.join(__dirname ,"..","models","friend"));
var Message = sequelize.import(path.join(__dirname ,"..","models","message"));
var Notice = sequelize.import(path.join(__dirname ,"..","models","notice"));
var Review = sequelize.import(path.join(__dirname ,"..","models","review"));
var Review_good = sequelize.import(path.join(__dirname ,"..","models","review_good"));
var Subject_list = sequelize.import(path.join(__dirname ,"..","models","subject_list"));
var Tutor = sequelize.import(path.join(__dirname ,"..","models","tutor"));
var Tutor_subject = sequelize.import(path.join(__dirname ,"..","models","tutor_subject"));
var Tutor_url = sequelize.import(path.join(__dirname ,"..","models","tutor_url"));
var User = sequelize.import(path.join(__dirname ,"..","models","user"));

Users.hasOne(Tutor, {foreignKey : 'id'})
Users.hasOne(User, {foreignKey : 'id'})
var user_users = User.belongsTo(Users,{foreignKey:'id',as:'Users'})
Follow.belongsTo(Users, {foreignKey : 'feeder'})
Friend.belongsTO(Users,{foreignKey : 'id'})
// Users.hasOne(Follow, {foreignKey : 'feeder',targetKey:"id"})
Users.hasMany(Tutor_subject,{foreignKey : 'id'});
User.belongsTo(Users,{foreignKey:"id",as:"Users"})
// Users.hasMany(Friend,{foreignKey : 'Friend1'});
// var recommends = Friend.hasMany(Friend,{foreignKey : 'friend1',targetKey:'friend2',as:'recommends'});
Tutor_subject.belongsTo(Subject_list,{foreignKey:'subjectCode'})
Users.hasMany(Tutor_subject,{foreignKey : 'id'});
Tutor.hasMany(Course,{foreignKey:"id"})
Course.hasMany(Course_subject,{foreignKey:"grp"})
Course.hasMany(Review,{foreignKey:"grp"})
Review.hasMany(Review_good,{foreignKey:"grp"})
Board.hasMany(Board_good,{foreignKey:'grp'})
Board.hasMany(Board_comment,{foreignKey:'grp'})
Board.hasMany(Board_photo,{foreignKey:'grp'})
// Tutor.belongsTo(Users)
// event handling:


sequelize.sync().then(function() {
    // ok ... everything is nice!
}).catch(function(error) {
    console.log(error);
    // oooh, did you enter wrong database credentials?
});

// Users.findById(10,{
//   include :
//   [
//     {model:Tutor}
//     ,{model:Tutor_subject,separate:true,include:[Subject_list]}
//   ]
// })
// .
// then(function(result){
//   result = JSON.parse(JSON.stringify(result));
//   console.log(result);
// })
exports.sequelize = sequelize;
exports.Users = Users;
exports.Alarm = Alarm;
exports.Board = Board;
exports.Board_comment = Board_comment;
exports.Board_good = Board_good;
exports.Board_photo = Board_photo;
exports.Career = Career;
exports.Course = Course;
exports.Course_subject = Course_subject;
exports.Follow = Follow;
exports.Friend = Friend;
exports.Message = Message;
exports.Notice = Notice;
exports.Review = Review;
exports.Review_good = Review_good;
exports.Subject_list = Subject_list;
exports.Tutor = Tutor;
exports.Tutor_subject = Tutor_subject;
exports.Tutor_url = Tutor_url;
exports.User = User;
// Users
//     .create({ email: 'email1@email.com', pw: '123p', isTutor:"1",last_name:"김",first_name:"성민",thumbnail:"asdf",photo:"asdf",
//             address_si:"의정부",address_gu:"의정부2동",gender:"1",about_me:"김성민입니다."})
//     .then(function() {
//         Users
//             .findOne({where: {email: 'email1@email.com'}})
//             .then(function (project) {
//                 console.log(project);
//             })
//     });
