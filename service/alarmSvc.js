var msg = require('./msg');
var sequelize = require("./sequelizes").sequelize;
var db = require("./sequelizes");
var sql = require("../models/sql.js");
var toData = require("../module/func").toDataSequelize;


//1번 카테고리 : 쪽지
//2번 카테고리 : 친구요청
//3번 카테고리 : 친구요청 수락
//4번 카테고리 : 친구요청 거절
var add = function(data,t){
  var alarm = {};
  alarm.receiver = data.receiver;
  alarm.cate = data.cate;
  if(data.board) alarm.board = data.board;
  if(data.course) alarm.course = data.course;
  if(data.actor) alarm.actor = data.actor;

  return db.Alarm.create(alarm,{transaction : t})
}
