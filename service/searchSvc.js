/**
 * Created by Administrator on 2015-02-04.
 */
var easyimg = require('easyimage');
var path = require('path');
var fs = require('fs');
var fse = require('fs-extra');
var async = require('async');
var sqls = require('./sqls.js');
var errors = require('../module/errors');

exports.searchTutee = function(data){
  var page = data.page || 1;
  var id =data.id;
  var keyword = '%'+data.keyword+'%';
  var size = 20;
  var offset = (page-1) * 20;


  return db.Users.findById(
    id,
    {
      attributes :
      {
        exclude: ['pw','salt']
      }
    }
  )
  .then(function(user){
    return sequelize.query(
      sqls.searchTutee
      ,{ bind: {address_si : user.address_si,id : user.id,keyword : keyword,school_name : user.school_name}, type: sequelize.QueryTypes.SELECT })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
};

exports.searchTutor = function(data){
  var id =data.id;
  var keyword = '%'+data.keyword+'%';
  var size = 20;
  var offset = (data.page-1) * 20;


  return db.Users.findById(
    id,
    {
      attributes :
      {
        exclude: ['pw','salt']
      }
    }
  )
  .then(function(user){
    return sequelize.query(
      sqls.searchTutor
      ,{ bind: {address_si : user.address_si,id : user.id,keyword : keyword}, type: sequelize.QueryTypes.SELECT })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}



exports.searchCourse = function(data){
  var id =data.id;
  var keyword = '%'+data.keyword+'%';
  var size = 20;
  var offset = (data.page-1) * 20;


  return db.Course.findAll({
    attributes :[{
      include:[sequelize.fn('COUNT',sequelize.col('review.idx')),'reviewCount']
    }],
    where :{
      $or :{
        title : { $like : keyword },
        content : { $like : keyword }
      }
    },
    include : [
      {model : db.Review,attributes:[]}
    ],
    order : ['regdate','desc'],
    offset : offset,
    limit : size
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.searchBoard = function(data){
  var id =data.id;
  var keyword = '%'+data.keyword+'%';
  var size = 20;
  var offset = (data.page-1) * 20;


  return db.Board.findAll({
    attributes : [{
      include:[sequelize.fn('COUNT',sequelize.col('board_comment.id')),'goodCount']
    }],
    where :{
      $or :{
        title : { $like : keyword },
        content : { $like : keyword }
      }
    },
    include : [
      {model : db.Board_comment,attributes:[]},
      {model : db.Board_photo,attributes:["grp_num","photo"]}
    ],
    order : ['regDate','desc'],
    offset : offset,
    limit : size
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}
