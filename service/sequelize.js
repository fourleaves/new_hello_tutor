var Sequelize = require("sequelize");
var path = require("path");
var nconf = require("nconf");
var sequelizeData = nconf.get("mysql");
var sequelize = new Sequelize(
  sequelizeData.database,
  sequelizeData.user,
  sequelizeData.password,
  {
    dialect: 'mysql',
    pool : sequelizeData.pool,
    define: {
        timestamps: false
    }
  }
);

var Users = sequelize.import(path.join(__dirname ,"..","models","users"));
var Alarm_relation = sequelize.import(path.join(__dirname ,"..","models","alarm_relation"));
var Board = sequelize.import(path.join(__dirname ,"..","models","board"));
var Board_comment = sequelize.import(path.join(__dirname ,"..","models","board_comment"));
var Board_good = sequelize.import(path.join(__dirname ,"..","models","board_good"));
var Board_photo = sequelize.import(path.join(__dirname ,"..","models","board_photo"));
var Career = sequelize.import(path.join(__dirname ,"..","models","career"));
var Course = sequelize.import(path.join(__dirname ,"..","models","course"));
var Course_subject = sequelize.import(path.join(__dirname ,"..","models","course_subject"));
var Follow = sequelize.import(path.join(__dirname ,"..","models","follow"));
var Message = sequelize.import(path.join(__dirname ,"..","models","message"));
var Notice = sequelize.import(path.join(__dirname ,"..","models","notice"));
var Review = sequelize.import(path.join(__dirname ,"..","models","review"));
var Review_good = sequelize.import(path.join(__dirname ,"..","models","review_good"));
var Subject_list = sequelize.import(path.join(__dirname ,"..","models","subject_list"));
var Tutor = sequelize.import(path.join(__dirname ,"..","models","tutor"));
var Tutor_subject = sequelize.import(path.join(__dirname ,"..","models","tutor_subject"));
var Tutor_url = sequelize.import(path.join(__dirname ,"..","models","tutor_url"));
var User = sequelize.import(path.join(__dirname ,"..","models","user"));

// event handling:
sequelize.sync().then(function() {
    // ok ... everything is nice!
}).catch(function(error) {
    console.log(error);
    // oooh, did you enter wrong database credentials?
});

// Users
//     .create({ email: 'email1@email.com', pw: '123p', isTutor:"1",last_name:"김",first_name:"성민",thumbnail:"asdf",photo:"asdf",
//             address_si:"의정부",address_gu:"의정부2동",gender:"1",about_me:"김성민입니다."})
//     .then(function() {
//         Users
//             .findOne({where: {email: 'email1@email.com'}})
//             .then(function (project) {
//                 console.log(project);
//             })
//     });
