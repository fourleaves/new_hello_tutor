var photoSvc = require('./photoSvc');
var sequelize = require("./sequelizes").sequelize;
var db = require("./sequelizes");
var sql = require("../models/sql.js");
var toData = require("../module/func").toDataSequelize;
var errors = require('../module/errors');

exports.createBoard = function(data,files){
  var boardInfo = {};
  boardInfo.id = data.id;
  boardInfo.title = data.title;
  boardInfo.content = data.content;
  boardInfo.video1 = data.video1;
  boardInfo.video2 = data.video2;
  boardInfo.url1 = data.url1;
  boardInfo.url2 = data.url2;

  photoInfo = {};
  photoInfo.Info = [];
  return sequelize.transaction(function(t){
      return new Promise(function(reslove,reject){
        photoSvc.uploadfunc_board(files,function(err,photo_info){
            if(err) reject(err);
            else reslove(photo_info);
        })
      })
      .then(function(photo_infos){
        photoInfo.Info = photoInfo;
        return db.Board.create(boardInfo, {transaction: t});
      })
      .then(function(board){
        var photoData = photoInfo.Info.map(function(item){
          rtn = {};
          rtn.grp = board.idx;
          rtn.grp_num = photoInfo.Info.indexOf(item);
          rtn.photo = item.photo;

          return rtn;
        });
        return db.Board_photo.bulkCreate(photoData,{transaction :t})
      })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.modifyBoard = function(data){
  var idx = data.idx;
  var boardInfo = {};
  boardInfo.id = data.id;
  boardInfo.title = data.title;
  boardInfo.content = data.content;
  boardInfo.video1 = data.video1;
  boardInfo.video2 = data.video2;
  boardInfo.url1 = data.url1;
  boardInfo.url2 = data.url2;

  photoInfo = {};
  photoInfo.Info = [];
  return sequelize.transaction(function(t){
      return new Promise(function(reslove,reject){
        photoSvc.uploadfunc_board(files,function(err,photo_info){
            if(err) reject(err);
            else reslove(photo_info);
        })
      })
      .then(function(photo_infos){
        photoInfo.Info = photoInfo;
        return db.Board.update(boardInfo,{where : {idx : idx}}, {transaction: t});
      })
      .then(function(board){
        return db.Board_photo.destroy({where : { idx : idx}});
      })
      .then(function(board){
        var photoData = photoInfo.Info.map(function(item){
          rtn = {};
          rtn.grp = idx;
          rtn.grp_num = photoInfo.Info.indexOf(item);
          rtn.photo = item.photo;

          return rtn;
        });
        return db.Board_photo.bulkCreate(photoData)
      })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
};

exports.deleteBoard = function(data){
  var boardInfo = {};
  boardInfo.idx = data.idx;

  return db.Board.destroy({where : {idx : boardInfo.idx}})
  .catch(function(){
    return promise.reject(new Errors());
  })
};


exports.createComment = function(data){
  var commentInfo = {};
  commentInfo.grp = data.grp;
  commentInfo.id = data.id;
  commentInfo.content = data.content;
  return db.Review_comment.findOne({
    where : {
      grp : commentInfo.grp
    }
    ,order : 'grp_num'
  })
  .then(function(Info){
    commentInfo.grp_num = Info.grp_num || 1;
    return db.Board_comment.create(commentInfo)
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
};


exports.modifyComment = function(data){
  var idx = data.idx
  var commentInfo = {};
  // reviewInfo.idx = data.idx;
  commentInfo.title = data.title;
  commentInfo.content = data.content;

  return db.Board_comment.update(commentInfo,{where : {idx : data.idx}});
};


exports.deleteComment = function(data){
  var boardInfo = {};
  boardInfo.idx = data.idx;

  return db.Board_comment.destroy({where : { idx : data.idx}})
};



exports.goodComment = function(data){
  var goodInfo = {};
  goodInfo.idx = data.idx;
  goodInfo.id = data.id;
  return db.Board_good.create(goodInfo);
};


exports.delGoodComment = function(data){
  var goodInfo = {};
  goodInfo.idx = data.idx;
  goodInfo.id = data.id;
  return db.Board_good.destroy({where : goodInfo});
};
