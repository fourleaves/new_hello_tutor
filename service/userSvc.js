var crypto = require('crypto');
var photoSvc = require('./photoSvc');
var alarmSvc = require('./alarmSvc');
var msg = require('./msg');
var sequelize = require("./sequelizes").sequelize;
var db = require("./sequelizes");
var sql = require("../models/sql.js");
var toData = require("../module/func").toDataSequelize;
var errors = require('../module/errors');


exports.signupTutor = function(data,files){
  var salt = Math.round((new Date().valueOf() * Math.random())) + '';
  var cryp_pass = crypto.createHash('sha512').update(data.pw + salt).digest('hex');
  data.pw = cryp_pass;
  data.salt = salt;

  return sequelize.transaction(function(t){
      return new Promise(function(reslove,reject){
        photoSvc.uploadfunc_join(files,data.email,function(err,photo_info){
            if(err) reject(err);
            else reslove(photo_info);
        })
      })
      .then(function(photo_info){
        data.photo = photo_info.photo;
        data.thumbnail = photo_info.thumbnail;
        return db.Users.create(data, {transaction: t});
      })
      .then(function(users){
        return db.Tutor.create({id:users.id},{transaction:t});
      })
      .then(function(user){
        var insertObj = [];
        data.subjects.map(function(item){
          var obj = {};
          obj.id = user.id;
          obj.subjectCode = item;
          insertObj.push(obj);
        })
        return db.Tutor_subject.bulkCreate(insertObj,{transaction:t});
      })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.signupTutee = function(data,files){
  var salt = Math.round((new Date().valueOf() * Math.random())) + '';
  var cryp_pass = crypto.createHash('sha512').update(data.pw + salt).digest('hex');
  data.pw = cryp_pass;
  data.salt = salt;

  return sequelize.transaction(function(t){
      return new Promise(function(reslove,reject){
        photoSvc.uploadfunc_join(files,data.email,function(err,photo_info){
            if(err) reject(err);
            else reslove(photo_info);
        })
      })
      .then(function(photo_info){
        data.photo = photo_info.photo;
        data.thumbnail = photo_info.thumbnail;
        return db.Users.create(data, {transaction: t});
      })
      .then(function(users){
        return db.Tutee.create(
          {
            id:users.id
            ,school_name:data.school_name
            ,grade:data.grade
          },{transaction:t});
      })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}





exports.login = function(data){
  var retValue = null;
  return db.Users.findOne({email:data.eamil})
  .then(function(users){
    users = users.dataValues;
    console.log(users);
    return new Promise(function(resolve,reject){
      if(users.pw != crypto.createHash('sha512').update(data.pw + users.salt).digest('hex')){
        reject("password is wrong!");
      }
      else{
        resolve(users);
      }
    })
  })
  .then(function(users){
    if(isTutor == 0) getTuteeInfo(users.id);
    else return getTutorInfo(users.id);
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
};


exports.recommendUser = function(data,callback){
    //values = email, page
  var id = data.id;
  var limitNum = (data.page-1)*10;
  var size = 20;
  //recommend user 1명 가져오기

  return sequelize.query(sql.recommendUser,{bind:{id:id,limitNum:limitNum,size:size}, type: sequelize.QueryTypes.SELECT})
  .then(function(recommends){
    return new Promise(function(resolve,reject){
      if(recommends.dataValues){
        var rtn = {};
        rtn.list = recommends.dataValues;
        resolve(rtn);
      }
      else{
        resolve(new Error("no result"));
      }
    })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
};

exports.recommendTutor = function(data,callback){
    //values = email, page
    var id = data.id;
    var limitNum = (data.page-1)*10;
    var size = 20;
    //recommend user 1명 가져오기

    return sequelize.query(sql.recommendUser,{bind:{id:id,limitNum:limitNum,size:size}, type: sequelize.QueryTypes.SELECT})
    .then(function(recommends){
      return new Promise(function(resolve,reject){
        if(recommends.dataValues){
          var rtn = {};
          rtn.list = recommends.dataValues;
          resolve(rtn);
        }
        else{
          resolve(new Error("no result"));
        }
      })
    })
    .catch(function(){
      return promise.reject(new Errors());
    })
};

//선생님 기본정보 + 수업하나 + 친구 + 할로워
exports.getTutorSimple = function(data){
  var targetId = data.targetId;
  return Promise.all([
    getTutorInfo(targetId),
    getFriendsWithPhoto(targetId),
    getFollowWithPhoto(targetId),
    getCourseOne(targetId)
  ])
  .then(function(values){
    var info = values[0];
    var friends = values[1];
    var follower = values[2];
    var course = values[3];
    info.friends = friends;
    info.follower = follower;
    info.course = course;
    return promise.resolve(info);
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}


exports.getTutorDetail = function(data){
  var targetId = data.targetId;
  return getTutorDetail(targetId)
}


//선생님 기본정보 + 수업하나 + 친구 + 할로워
exports.getTuteeSimple = function(data){
  var targetId = data.targetId;
  return Promise.all([
    getTuteeInfo(targetId),
    getFriendsWithPhoto(targetId),
    getFollowWithPhoto(targetId),
    getBoardOne(targetId)
  ])
  .then(function(values){
    var info = values[0];
    var friends = values[1];
    var follower = values[2];
    var board = values[3];
    info.friends = friends;
    info.follower = follower;
    info.board = course;
    return promise.resolve(info);
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

//선생님 기본정보 + 수업하나 + 친구 + 할로워
exports.writeCareer = function(data){
  return db.Career.create(data);
}

exports.deleteCareer = function(data){
  return db.Career.destroy({
    where : {
      id : data.id,
      idx : data.idx
    }
  });
}

exports.modifyCareer = function(data){
  var careerData = {};
  careerData.title = data.title;
  careerData.fromDate = data.fromDate;
  careerData.toDate = data.toDate;
  careerData.content = data.content;

  return db.Career.update(careerData,{
    where : {
      id : data.id,
      idx : data.idx
    }
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.deleteSubject = function(data){
  return db.Tutor_subject.destroy({
    where : {
      id : data.id,
      subjectCode : data.subjectCode
    }
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.addSubject = function(data){
  return db.Tutor_subject.create(data);
}

exports.checkEmail = function(data){
  return db.Users.findOne({
    email : data.email
  })
  .then(function(result){
    var rtn = {};
    if(result && result.length == 0){
      rtn.result = true;
    }
    else{
      rtn.result = false;
    }
    return promise.resolve(rtn);
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.follow = function(data){
  var followInfo = {};
  followInfo.follower = data.follower;
  followInfo.feeder = data.feeder;
  return db.Follow.create(followInfo)
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.unfollow = function(data){
  var followInfo = {};
  followInfo.follower = data.follower;
  followInfo.feeder = data.feeder;
  return db.Follow.destroy({where : followInfo})
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.reqFriend = function(data){
  var frinedInfo = {};
  frinedInfo.friend1 = data.id;
  frinedInfo.friend2 = data.friend;
  return sequelize.transaction(function(t){
    return db.Friend.create(friendInfo,{transaction : t})
    .then(function(friendInfo){
      var alarmInfo = {};
      alarmInfo.receiver = data.friend;
      alarmInfo.actor = data.id;
      alarmInfo.cate = 2;
      return alarmSvc.add(alarmInfo,{transaction : t});
    })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.acceptFriend = function(data){
  var frinedInfo = {};
  frinedInfo.friend2 = data.id;
  frinedInfo.friend1 = data.friend;
  return sequelize.transaction(function(t){
    return db.Friend.update({agree : 'y'},{where : friendInfo},{transaction : t})
    .then(function(friendInfo){
      var alarmInfo = {};
      alarmInfo.receiver = data.friend;
      alarmInfo.actor = data.id;
      alarmInfo.cate = 3;
      return alarmSvc.add(alarmInfo,{transaction : t});
    })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.rejectFriend = function(data){
  var frinedInfo = {};
  frinedInfo.friend2 = data.id;
  frinedInfo.friend1 = data.friend;
  return sequelize.transaction(function(t){
    return db.Friend.destroy({where : friendInfo},{transaction : t})
    .then(function(friendInfo){
      var alarmInfo = {};
      alarmInfo.receiver = data.friend;
      alarmInfo.actor = data.id;
      alarmInfo.cate = 4;
      return alarmSvc.add(alarmInfo,{transaction : t});
    })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.getFriendList= function(data){
  return db.Friend.findAll({
    where : {
      $or : {
        friend1 : data.id,
        friend2 : data.id
      }
    }
  })
  .then(function(friends){
    friends = toDate(friends);
    friendIds = friends.map(function(item){
      var friend = null;
      if(data.id == item.friend1)
        friend = item.friend2;
      else if(data.id == item.friend2)
        friend = item.friend1;
      return friend;
    })
    return promise.resolve(friendIds);
  })
  .then(function(friendIds){
    return db.Users.findAll({where : {id : friendIds}});
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.getFollowList= function(data){
  return db.Friend.findAll({
    attributes : {
      include : [
        ['feeder','id'],
        [sequelize.col("Users.first_name"),'first_name'],
        [sequelize.col("Users.last_name"),'last_name'],
        [sequelize.col("Users.address_gu"),'address_gu'],
        [sequelize.col("Users.address_si"),'address_si'],
        [sequelize.col("Users.email"),'email']
      ]
    }
    ,where : {
        follower : data.id
    }
    ,include : [
      {model : db.Users, attributes : []}
    ]
  })
}
//선생님 기본정보 + 과목
var getTutorInfo = function(id){

  return db.Users.findById(
    id,
    {
      attributes :
      {
        exclude: ['pw','salt']
        ,include:
        [
          [sequelize.col('Tutor.academic'),"academic"]
        ]
      },
      include : [
        {
          model : db.Tutor
          ,attributes : []
        },
        {
          model : Tutor_subject
          ,attributes : { exclude : "id"}
          , include : [
            {
              model : db.Subject_list
            }
          ]
        }
      ]
    }
  )
  .then(function(result){
    result = toData(JSON.stringify(result));
    result.subjects = result.tutor_subjects.map(function(subjectInfo){
     subjectInfo.subject = subjectInfo.subject_list.subject;
     delete(subjectInfo.subject_list);
     return subjectInfo;
    })
    return Promise.resolve(result);
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}



//선생님 기본정보 + 과목
var getTutorDetail = function(id){

  return db.Users.findById(
    id,
    {
      attributes :
      {
        exclude: ['pw','salt']
        ,include:
        [
          [sequelize.col('Tutor.academic'),"academic"]
        ]
      },
      include : [
        {
          model : db.Tutor
          ,attributes : []
        },
        {
          model : db.Tutor_url
        },
        {
          model : db.Career
        },
        {
          model : Tutor_subject
          ,attributes : { exclude : "id"}
          , include : [
            {
              model : db.Subject_list
            }
          ]
        }
      ]
    }
  )
  .then(function(result){
    result = toData(JSON.stringify(result));
    result.subjects = result.tutor_subjects.map(function(subjectInfo){
     subjectInfo.subject = subjectInfo.subject_list.subject;
     delete(subjectInfo.subject_list);
     return subjectInfo;
    })
    return Promise.resolve(result);
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

var getTuteeInfo = function(id){

  return db.Users.findById(
    id,
    {
      attributes :
      {
        exclude: ['pw','salt']
        ,include:
        [
          [sequelize.col('User.school_name'),"school_name"],
          [sequelize.col('User.grade'),"grade"]
        ]
      },
      include : [
        {
          model : db.User
          ,attributes : []
        }
      ]
    }
  )
  .then(function(result){
    result = JSON.parse(JSON.stringify(result));
    return Promise.resolve(result);
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

var getFriendsWithPhoto = function(id){
  var friend2 = db.Friend.belongsTo(db.Users,{foreignKey : 'friend2',targetKey : "id",as:"F2"})
  var friend1 = db.Friend.belongsTo(db.Users,{foreignKey : 'friend1',targetKey : "id",as:"F1"})

  return db.Friend.findAll({
        attributes : {
          include : [
            [sequelize.col("F1.photo"),"photo1"],
            [sequelize.col("F2.photo"),"photo2"]
          ]
        },
        where :{
          $or : [
            {friend2 : id},
            {friend1 : id}
          ]
        },
        include : [
          {model : db.Users,association : friend1,attributes:[]},
          {model : db.Users,association : friend2,attributes:[]}
        ],
        limit : 3
      }).then(function(friends){
        var rtn = friends.map(function(itme){
          var obj = {};
          if(item.friend1 == id){
            obj.id =  item.friend1;
            obj.photo = itme.photo1;
          }
          else{
            obj.id =  item.friend2;
            obj.photo = itme.photo2;
          }
          return obj;
        })
        return Promise.reslove(rtn);
      })
};


var getFollowWithPhoto = function(id){
  return db.Follow.findAll({
    attributes : {
      include : [
        [sequelize.col("user.photo"),"photo"]
      ],
      exclude : ["regdate"]
    },
    where :{
      follower : id
    },
    include : [
      {model : db.Users,attributes:[]}
    ],
    limit : 3
  })
};

var getCourseOne = function(id){
  return db.Course.findOne({
    attributes :[{
      include:[sequelize.fn('COUNT',sequelize.col('review.idx')),'reviewCount']
    }],
    where :{
      "id" : id
    },
    include : [
      {model : db.Review,attributes:[]}
    ],
    order : ['reviewCount','desc']
    // ,limit : 1
  })
};

var getBoardOne = function(id){
  return db.Board.findOne({
    attributes : [{
      include:[sequelize.fn('COUNT',sequelize.col('board_comment.id')),'goodCount']
    }],
    where :{
      "id" : id
    },
    include : [
      {model : db.Board_comment,attributes:[]},
      {model : db.Board_photo,attributes:["grp_num","photo"]}
    ],
    order : ['regDate','desc']
    // ,limit : 1
  })
};
