var photoSvc = require('./photoSvc');
var msg = require('./msg');
var sequelize = require("./sequelizes").sequelize;
var db = require("./sequelizes");
var sql = require("../models/sql.js");
var alarmSvc = require("./alarmSvc.js");
var toData = require("../module/func").toDataSequelize
var errors = require('../module/errors');

exports.send = function(data){

  var messageObj = {};
  messageObj.to = data.to;
  messageObj.from = data.from;
  messageObj.content = data.content;
  return sequelize.transaction(function(t){
    return db.Message.create(messageObj,{transaction : t})
    .then(function(message){
      var alarm = {};
      alarm.receiver = data.to;
      alarm.cate = 1;
      alarm.actor = data.from;
      return alarmSvc.add(alarm,t);
    })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.getList = function(data){
  return db.Message.findAll({
    receiver : data.id
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.read = function(data){
  return db.Message.update({
    read_yn : "y"
  },
  {
    where : { idx : data.idx }
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.notice = function(data){
  return db.Notice.getAll()
  .catch(function(){
    return promise.reject(new Errors());
  })
}
