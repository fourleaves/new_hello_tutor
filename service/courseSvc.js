var photoSvc = require('./photoSvc');
var sequelize = require("./sequelizes").sequelize;
var db = require("./sequelizes");
var sql = require("../models/sql.js");
var toData = require("../module/func").toDataSequelize;
var errors = require('../module/errors');
exports.createCourse = function(data,files){
  var courseInfo = {};
  courseInfo.id = data.id;
  courseInfo.title = data.title;
  courseInfo.content = data.content;
  courseInfo.one_line = data.one_line;

  return sequelize.transaction(function(t){
      return new Promise(function(reslove,reject){
        photoSvc.uploadfunc_course(files,function(err,photo_info){
            if(err) reject(err);
            else reslove(photo_info);
        })
      })
      .then(function(photo_info){
        data.photo = photo_info.photo;
        data.thumbnail = photo_info.thumbnail;
        return db.Course.create(data, {transaction: t});
      })
  })
  .catch(function(){
    return promise.reject(new Errors());
  })
}

exports.modifyCourse = function(data){
  var courseInfo = {};
  courseInfo.title = data.title;
  courseInfo.content = data.content;
  courseInfo.one_line = data.one_line;

  return sequelize.transaction(function(t){
      return new Promise(function(reslove,reject){
        photoSvc.uploadfunc_course(files,function(err,photo_info){
            if(err) reject(err);
            else reslove(photo_info);
        })
      })
      .then(function(photo_info){
        data.photo = photo_info.photo;
        data.thumbnail = photo_info.thumbnail;
        return db.Course.update(data,{where : {idx : data.idx}}, {transaction: t});
      })
    })
    .catch(function(){
      return promise.reject(new Errors());
    });

};

exports.deleteCourse = function(data){
  var courseInfo = {};
  courseInfo.idx = data.idx;

  return db.Course.destroy({where : {idx : courseInfo.idx}})
  .catch(function(){
    return promise.reject(new Errors());
  })
};


exports.createReview = function(data){
  var reviewInfo = {};
  reviewInfo.grp = data.grp;
  reviewInfo.id = data.id;
  reviewInfo.title = data.title;
  reviewInfo.content = data.content;

  return db.Review.create(reviewInfo)
  .catch(function(){
    return promise.reject(new Errors());
  })
};


exports.modifyReview = function(data){
  var reviewInfo = {};
  // reviewInfo.idx = data.idx;
  reviewInfo.title = data.title;
  reviewInfo.content = data.content;

  return db.Review.update(reviewInfo,{where : {idx : data.idx}})
  .catch(function(){
    return promise.reject(new Errors());
  })

};


exports.deleteReview = function(data){
  var reviewInfo = {};
  reviewInfo.idx = data.idx;

  return db.Review.destroy({where : { idx : data.idx}})
  .catch(function(){
    return promise.reject(new Errors());
  })
};



exports.goodReview = function(data){
  var goodInfo = {};
  goodInfo.idx = data.idx;
  goodInfo.id = data.id;
  return db.Review_good.create(goodInfo)
  .catch(function(){
    return promise.reject(new Errors());
  })
};


exports.delGoodReview = function(data){
  var goodInfo = {};
  goodInfo.idx = data.idx;
  goodInfo.id = data.id;
  return db.Review_good.destroy({where : goodInfo})
  .catch(function(){
    return promise.reject(new Errors());
  })
};
